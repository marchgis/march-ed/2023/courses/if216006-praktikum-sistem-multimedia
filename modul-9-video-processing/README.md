# Video Processing with Python

## Mengakses Video dari Kamera
- [materi - menggunakan Google Colab](https://colab.research.google.com/drive/1ikW3t-qXmQ6ZZMtQgdY-cJX9xbvlKzVt?usp=sharing)
- [materi - menggunakan Python script](https://www.geeksforgeeks.org/python-opencv-capture-video-from-camera/)
- [materi - menggunakan Javascript via web](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream_Recording_API/Recording_a_media_element)

## Editing Video
[materi](https://www.codespeedy.com/cut-or-trim-a-video-using-moviepy-in-python/)

## Ekstraksi Image dari Video
[materi](https://www.geeksforgeeks.org/python-process-images-of-a-video-using-opencv/)

## Ekstraksi Audio dari Video
[materi](https://www.codespeedy.com/extract-audio-from-video-using-python/)

## Membuat Video dari Image dan Audio
[materi](https://www.thepythoncode.com/article/add-static-image-to-audio-in-python?ref=morioh.com&utm_source=morioh.com)

## Membuat Animasi di Browser
- [Three.js](https://threejs.org/) [galeri contoh](https://threejs.org/examples/)
- [PixiJS](https://pixijs.com/) [galeri contoh](https://pixijs.com/gallery/)
- [p5js](https://p5js.org/) [galeri contoh](https://p5js.org/examples/)

## Referensi
### Tools
- [Python - moviepy](https://pypi.org/project/moviepy/)
- Javascript
    - Three.js
    - PixiJS
    - p5js

### Artikel Terkait
- [Recording a media element](https://developer.mozilla.org/en-US/docs/Web/API/MediaStream_Recording_API/Recording_a_media_element)
- [Process images of a video using OpenCV - GeeksforGeeks](https://www.geeksforgeeks.org/python-process-images-of-a-video-using-opencv/)
- [How to extract audio from video in Python - Codespeedy](https://www.codespeedy.com/extract-audio-from-video-using-python/)
- [How to Combine a Static Image with Audio in Python](https://www.thepythoncode.com/article/add-static-image-to-audio-in-python?ref=morioh.com&utm_source=morioh.com)
