# Pitching 1

## 1. Produk Berbasis Video
1. Rancang produk digital berbasis pemrosesan video yang didalamnya mengandung unsur:
    - Video processing
    - Video compression
    - Video AI (opsional)
2. Buat penjelasan dari produk tersebut
3. Desain dan implementasikan dalam bentuk program sederhana (CLI / Web / Mobile)
4. Buat video presentasi dari produk yang dibuat (Youtube audience publik)
5. Wawancara: mampu menjelaskan konsep video processing, video compression, & video processing tool development

## 2. Research and Development Berbasis Image / Audio / Video
1. Rancang inovasi berbasis Image / Audio / Video, boleh berdasarkan produk yang dibuat sebelumnya yang didalamnya berisi :
    - Multimedia processing
    - Multimedia compression
    - Pilih:
        - Multimedia AI (recognition / generation)
        - Multimedia in distributed computing
        - Multimedia online tools
2. Dokumentasikan rujukan-rujukan terkait dari (IEEE / Arxiv / ScienceDirect / ACM)
3. Desain dan implementasikan dalam bentuk program / model (Model / CLI / Web / Mobile)
4. Buat video presentasi dari produk yang dibuat (Youtube audience publik)
5. Wawancara: mampu menjelaskan konsep 


