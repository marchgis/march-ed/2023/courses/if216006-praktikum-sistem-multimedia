# Generative Machine Learning

## Referensi
- [Generative Adversarial Networks: Build Your First Models](https://realpython.com/generative-adversarial-networks/)
- [How to Develop a GAN for Generating MNIST Handwritten Digits](https://machinelearningmastery.com/how-to-develop-a-generative-adversarial-network-for-an-mnist-handwritten-digits-from-scratch-in-keras/)
- [Auto A - Stable Diffusion Text to Image](https://github.com/Stability-AI/stablediffusion)
- [Open Source Text to Speech](https://harishgarg.com/writing/best-open-source-text-to-speech-tools/)
