# IF216006 - Praktikum Sistem Multimedia

## Learning Outcome Outline
- Multimedia concept
- Multimedia representation and processing
    - OpenCV
    - Image steganography
- Multimedia Compression
    - Huffman code
    - Arithmetic encoding
    - Image compression
        - KMeans
- Multimedia distribution
- Multimedia processing for deep learning

## Pertemuan

Minggu ke | Learning Outcome | Materi & Asesmen
---|---|---
1 | <ul><li>Mampu menggunakan Google Colaboratory</li><li> Mampu menggunakan dasar pemrograman Python</li></ul> | <ul><li>[🦓 Dasar Google Colab](https://gitlab.com/marchgis/march-ed/2023/courses/google-colab/)</li><li>[🐍 Dasar Python](https://gitlab.com/marchgis/march-ed/2023/courses/dasar-python/)</li></ul>
2 | Image representation algoritms | [Materi modul 2](./modul-2)
3 | Image compression algoritms | <ul><li><a href="https://gitlab.com/marchgis/march-ed/2023/courses/if216006-praktikum-sistem-multimedia/-/blob/main/image-commpression/kompresi-data.md">Kompresi Data</a></li><!--<li><a href="./modul-3">Praktikum modul 3</a></li>--></ul>
4 | Image manipulation softwares / cloud | [Materi modul 4](./modul-4-image-processing-tools)
5 | Audio representation and processing | [Materi modul 5](./modul-5-audio-representation)
6 | Audio compression algoritms | [Materi modul 6](./audio-compression/audio-compression.md)
7 | Audio manipulation softwares / cloud | [Materi modul 7](./modul-7-audio-processing-software)
8 | UTS |
9 | Video representation and processing | [Materi modul 9](./modul-9-video-processing)
10 | Video compression algoritms | [Materi modul 10](./modul-10-video-compression)
11 | Video manipulation softwares / cloud | [Materi modul 11](./modul-11-video-processing-software)
12 | RnD project - Intro: Discriminative machine learning for image / audio / video | [Materi modul 12](./modul-12-discriminative-machine-learning)
13 | RnD project - Intro: Generative machine learning for image / audio / video | [Materi modul 13](./modul-13-generative-machine-learning)
14 | RnD project - Machine learning experiment for image / audio / video | [Materi modul 14](./modul-14-machine-learning-experiment/README.md)
15 | RnD project |
16 | UAS |

## Tools
- Google Colaboratory
- Python
    - Numpy
    - Image
        - OpenCV
        - Pillow
        - Keras
            - ImageDataGenerator
    - Sound
        - Librosa
        - Soundfile

## Referensi

### Materi
- [](https://www.analyticsvidhya.com/blog/2021/09/ok-google-speech-to-text-in-python-with-deep-learning-in-2-minutes/)

### Kurikulum
- [KAIST - EE474 Introduction to Multimedia](https://sites.google.com/site/kaistee474itm/syllabus)
