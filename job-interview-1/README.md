# Job Interview 1

## 1. Produk Berbasis Image
1. Rancang produk digital berbasis pemrosesan image yang didalamnya mengandung unsur:
    - Image processing
    - Image compression
2. Buat penjelasan dari produk tersebut
3. Desain dan implementasikan dalam bentuk program sederhana (CLI / Web / Mobile)
4. Buat video presentasi dari produk yang dibuat (Youtube audience publik)
5. Wawancara: mampu menjelaskan konsep image processing, image compression, & image processing tool development

## 2. Produk Berbasis Audio
1. Rancang produk digital berbasis pemrosesan audio yang didalamnya mengandung unsur:
    - Audio processing
    - Audio compression
2. Buat penjelasan dari produk tersebut
3. Desain dan implementasikan dalam bentuk program sederhana (CLI / Web / Mobile)
4. Buat video presentasi dari produk yang dibuat (Youtube audience publik)
5. Wawancara: mampu menjelaskan konsep audio processing, audio compression, & audio processing tool development

