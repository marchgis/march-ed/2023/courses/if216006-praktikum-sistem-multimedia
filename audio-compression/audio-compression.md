# Audio Compression Menggunakan Python 
## Implementasi Audio Compression Menggunakan Metode Downsampling 

File digital yang kita kenal seperti dokumen pdf, Gambar dengan berbagai format kita mengenal format audio. Terkadang file tersebut membutuhkan prosesing diantarnya cara compressi, compressi pada file audio salah satunya di kenal dengan metode downsampling. 

Ide utama dari konsep ini ialah menggunakan proses dimana audio terdiri dari data yang diambil sampelnya secara berkala, namun ukuran file audio bertambah seiring dengan bertambahnya jumlah sampel yang dimilikinya, sehingga mengambil lebih sedikit sampel, dan dengan demikian mengubah periodisitasnya, memungkinkan kita memiliki file yang lebih kecil.file .mp3 adalah file audio yang paling umum, kita akan mulai dari file .mp3, yang akan kita proses dengan Python.

## Converting .mp3 to a .wav file 

Cara yang pertama kila akan mengkonversi file .mp3 kedalam format .wav, untuk memproses bagian ini kita perlu mengakses library python soundfile. untuk langkah yang sederhana gunakan durasi audio yang tidak terlalu panjang, file audio yang kurang dari 5 menit menghindari proses running tidak memerlukan waktu yang cukup banyak. 

untuk mengkonversi file audio kita gunakan library pydub. kita dapat menginstal pydub menggunakan. code sebagai berikut : 

```py
!pip install pydub
```
```py
from pydub import AudioSegment

mp3File = "source_file.mp3"
wavFile = "source_file.wav"

# Convert .wav to .mp3                                                            
audio = AudioSegment.from_mp3(mp3File)
audio.export(wavFile, format="wav")
```
## Mengambil data dari .wav

```py
!pip install soundfile 
```
lalu, mari kita ambil data audionya :

```py
import soundfile as sf

# Retrieve the data from the wav file
data, samplerate = sf.read(wavFile)
```
- Data adalah 2D-array (dengan shape(n,2) dimana n adalah integer), yang memiliki audio data 
- Samplerate ialah sampel rate adalah number dari sample audio per second

kita dapat menampilkan sampel rate audio mengunakan code berikut 

```py
print("Sample rate : {} Hz".format(samplerate))
```

Mungkin kalian akan melihat nilai 48 000 Hz, yang mana memungkinan ini dapat berjalan secara normal karena 48kHz merupakan standar sampling rate. 

Sebelum kita mulai menggunakan dengan data audio, Anda mungkin bertanya-tanya apa yang direpresentasikan secara fisik oleh data tersebut. Nyatanya, suara sesuai dengan variasi tekanan udara, yang kemudian diubah menjadi voltase, untuk diproses dengan komputer. Dengan demikian data audio tersebut dapat dilihat sebagai variasi tekanan udara, atau voltase. Bagaimanapun, tidak penting untuk mengetahui bahwa mengompres file audio.

## Fourier Analysis

```py
import numpy as np
n = len(data) #panjang data array yang dimiliki data 
Fs = samplerate #sample rate
# menggunakanstereo audio, terdapat dua channel yang ada di data
# mendapatkan data dari masing masing channel secara terpisah:
ch1 = np.array([data[i][0] for i in range(n)]) #channel 1
ch2 = np.array([data[i][1] for i in range(n)]) #channel 2
```
setiap channel ch1 dan ch2 memiliki sample dari signal lanjutannya yang di dapatkan daru sampel rate Fs, artinya itu adalah nilai dari sinyal waktu kontinu yang diambil setiap $1/𝐹_𝑠$ detik. Secara matematis, jika 𝑥(𝑡) adalah sinyal kontinu waktu dari mana kita mengambil sampel, dan $𝑥̃ [𝑘]$ adalah sinyal sampel (jadi ch1 di sini misalnya), maka kita memiliki, untuk semua $0≤𝑘<𝑛$, $𝑥̃ [𝑘]=𝑥(𝑘/𝐹𝑠)$.

kemudian dapat melakukan analisis Fourier pada saluran pertama untuk melihat seperti apa spektrumnya. Dalam pertimbangan teorema Nyquist-Shannon, kita hanya melihat frekuensi di bawah $𝐹_𝑠/2$

```py
import matplotlib.pyplot as plt
ch1_Fourier = np.fft.fft(ch1) #melakukan Fast Fourier Transform
abs_ch1_Fourier = np.absolute(ch1_Fourier[:n//2]) #spectrume 
plt.plot(np.linspace(0, Fs / 2, n//2), abs_ch1_Fourier)
plt.ylabel('Spectrum')
plt.xlabel('$f$ (Hz)')
plt.show()
```
## Downsampling Audio 
mari kita tentukan frekuensi $𝑓0$ di atasnya kita akan memotong spektrumnya, artinya kita hanya menyimpan frekuensi di bawah $𝑓0$ dalam file audio terkompresi. Untuk melakukan itu, kami memperkenalkan parameter $𝜀$ $(0<𝜀<1)$ yang mewakili bagian spektrum yang tidak kita pertahankan. Di sini, kita menggunakan $𝜀=1e-5$ untuk menghapus hanya sebagian kecil dari spektrum, tetapi kita dapat mengubah nilai $𝜀$untuk melihat bagaimana $𝑓0$ dan file audio keluaran berubah. Secara intuitif, saat $𝜀$ meningkat, kita menjaga frekuensi lebih sedikit sehingga kualitas audio lebih buruk, tetapi ukuran file keluaran lebih kecil.


```py
eps = 1e-5
# Booleran array dimana setiap nilai mengindikasinya untuk tetap kita jaga nilai frekuensinya
frequenciesToRemove = (1 - eps) * np.sum(abs_ch1_Fourier) < np.cumsum(abs_ch1_Fourier)
# frekuensi yang di potong dibagian spectrum
f0 = (len(frequenciesToRemove) - np.sum(frequenciesToRemove) )* (Fs / 2) / (n / 2)
print("f0 : {} Hz".format(int(f0)))
# menampilkan spectrum dengan garis vertikal untuk f0
plt.axvline(f0, color='r')
plt.plot(np.linspace(0, Fs / 2, n//2), abs_ch1_Fourier)
plt.ylabel('Spectrum')
plt.xlabel('$f$ (Hz)')
plt.show()
```
Kode di bawah mengimplementasikan downsampling file audio :

```py
#Pertama mendefinisikan nama dari output files
wavCompressedFile = "audio_compressed.wav"
mp3CompressedFile = "audio_compressed.mp3"

#Definisikan downsampling factor
D = int(Fs / f0)
print("Downsampling factor : {}".format(D))
new_data = data[::D, :] #mendapatkan sample data

#menulisakan data baru dalam formal wav file
sf.write(wavCompressedFile, new_data, int(Fs / D), 'PCM_16')

#converting kembali menggunakan format mp3
audioCompressed = AudioSegment.from_wav(wavCompressedFile)
audioCompressed.export(mp3CompressedFile, format="mp3")
```

Saat downsampling, biasanya terjadi loss dan distorsi, sehingga kualitas audio menurun. Oleh karena itu, ada trade-off antara kualitas audio dan penyimpanan.
Ada beberapa alasan mengapa kualitas audio memburuk:

- sebagai konsekuensi dari teorema pengambilan sampel Nyquist-Shannon, mungkin ada aliasing.
- frekuensi di atas setengah laju sampel baru tidak lagi berada dalam spektrum audio terkompresi.


code jupyternotebook :

[code-compression audio](https://colab.research.google.com/drive/1eoAcfq8GCnI4pnngshtCEuAw5HFiJkce#scrollTo=z4MbjSkoJvPw)


Refererensi 
- https://medium.com/@jmpion/simple-audio-compression-with-python-70bdd7535b0a
- https://github.com/jmpion/simple-audio-compression-with-python
- https://www.uio.no/studier/emner/matnat/math/MAT-INF1100/h07/undervisningsmateriale/kap6.pdf


